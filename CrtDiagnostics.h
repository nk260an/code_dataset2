// Copyright (c) Microsoft Corporation. All rights reserved.
//

#pragma once

namespace Agent
{
namespace Diagnostics
{
	class CCrtDiagnostics
	{
	public:
		static void Enable();
	}; // CBasicDiagnostics
}
}