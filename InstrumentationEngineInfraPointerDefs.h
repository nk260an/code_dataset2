// Copyright (c) Microsoft Corporation. All rights reserved.
//

#pragma once

struct IProfilerManagerLogging;
struct IProfilerManager;

typedef ATL::CComPtr<IProfilerManagerLogging> IProfilerManagerLoggingSptr;
typedef ATL::CComPtr<IProfilerManager> IProfilerManagerSptr;