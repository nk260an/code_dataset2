/*
 * Copyright (C) 2019 Intel Corporation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   * Neither the name of Intel Corporation nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef P11_PROVIDER_H
#define P11_PROVIDER_H

#include "p11Defines.h"
#include "GPFunctions.h"
#include "KeyManagement.h"
#include "SessionManagement.h"
#include "SlotTokenManagement.h"
#include "Parallel.h"
#include "Encryption.h"
#include "Decryption.h"
#include "Digest.h"
#include "SignVerify.h"
#include "ObjectManagement.h"

#define PKCS_API

// PKCS #11 function list
static CK_FUNCTION_LIST functionList =
{
    // Version information
    { CRYPTOKI_VERSION_MAJOR, CRYPTOKI_VERSION_MINOR },
    // Function pointers
    C_Initialize,
    C_Finalize,
    C_GetInfo,
    C_GetFunctionList,
    C_GetSlotList,
    C_GetSlotInfo,
    C_GetTokenInfo,
    C_GetMechanismList,
    C_GetMechanismInfo,
    C_InitToken,
    C_InitPIN,
    C_SetPIN,
    C_OpenSession,
    C_CloseSession,
    C_CloseAllSessions,
    C_GetSessionInfo,
    C_GetOperationState,
    C_SetOperationState,
    C_Login,
    C_Logout,
    C_CreateObject,
    C_CopyObject,
    C_DestroyObject,
    C_GetObjectSize,
    C_GetAttributeValue,
    C_SetAttributeValue,
    C_FindObjectsInit,
    C_FindObjects,
    C_FindObjectsFinal,
    C_EncryptInit,
    C_Encrypt,
    C_EncryptUpdate,
    C_EncryptFinal,
    C_DecryptInit,
    C_Decrypt,
    C_DecryptUpdate,
    C_DecryptFinal,
    C_DigestInit,
    C_Digest,
    C_DigestUpdate,
    C_DigestKey,
    C_DigestFinal,
    C_SignInit,
    C_Sign,
    C_SignUpdate,
    C_SignFinal,
    C_SignRecoverInit,
    C_SignRecover,
    C_VerifyInit,
    C_Verify,
    C_VerifyUpdate,
    C_VerifyFinal,
    C_VerifyRecoverInit,
    C_VerifyRecover,
    C_DigestEncryptUpdate,
    C_DecryptDigestUpdate,
    C_SignEncryptUpdate,
    C_DecryptVerifyUpdate,
    C_GenerateKey,
    C_GenerateKeyPair,
    C_WrapKey,
    C_UnwrapKey,
    C_DeriveKey,
    C_SeedRandom,
    C_GenerateRandom,
    C_GetFunctionStatus,
    C_CancelFunction,
    C_WaitForSlotEvent
};

static CK_MECHANISM_TYPE symmetricMechanisms[] =
{
    CKM_AES_KEY_GEN,
    CKM_AES_CTR,
    CKM_AES_GCM,
    CKM_AES_CBC,
    CKM_AES_CBC_PAD,
    CKM_AES_PBIND,
};

static CK_MECHANISM_TYPE asymmetricMechanisms[] =
{
    CKM_RSA_PKCS_KEY_PAIR_GEN,
    CKM_RSA_PKCS,
    CKM_SHA256_RSA_PKCS,
    CKM_SHA512_RSA_PKCS,
    CKM_RSA_PKCS_PSS,
    CKM_SHA256_RSA_PKCS_PSS,
    CKM_SHA512_RSA_PKCS_PSS,
    CKM_RSA_PBIND_EXPORT,
    CKM_RSA_PBIND_IMPORT,
    CKM_EXPORT_RSA_PUBLIC_KEY,
    CKM_IMPORT_RSA_PUBLIC_KEY,
    CKM_EXPORT_EPID_QUOTE_RSA_PUBLIC_KEY,
    CKM_EXPORT_ECDSA_QUOTE_RSA_PUBLIC_KEY,
};

static CK_MECHANISM_TYPE digestMechanisms[] =
{
    CKM_SHA256,
    CKM_SHA512,
    CKM_SHA256_HMAC_AES_KEYID,
    CKM_SHA512_HMAC_AES_KEYID,
};

#endif //P11_PROVIDER_H