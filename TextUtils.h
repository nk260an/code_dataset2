// Copyright (c) Microsoft Corporation. All rights reserved.
//

#pragma once

namespace Agent
{
namespace Text
{
namespace TextUtils
{
	bool IsEmpty(const std::wstring& strInput);

	const std::wstring& GetEmpty();
}
}
}